/**
 * @file main.h
 * @author Thomas FRION 
 * @brief Header de test
 * @version 0.1
 * @date 2019-02-01
 * 
 * @copyright Copyright (c) 2019
 * 
 */

/**
 * @brief Fonction de test
 * 
 * @return int 0
 */
int test();