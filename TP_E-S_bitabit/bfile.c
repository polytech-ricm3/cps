#include "bfile.h"
#include "bit_operations.h"


struct bfile_t {
  FILE* fichier;
  int bit_index;
  int eof;
  char tampon; //int8_t car sur 1 octet et notre tampon doit faire 8 bits
  char *mode;
};

BFILE *bstart(FILE *fichier, const char *mode)
{
  BFILE *file = NULL;
  if(strcmp(mode,ECRITURE) == 0 || strcmp(mode,LECTURE) == 0)
  {
    file = malloc(sizeof(BFILE));
    file->fichier = fichier;
    file->bit_index = 7;
    file->eof = 0;
    file->mode = mode;
    file->tampon = (!strcmp(mode,LECTURE)) ? fgetc(file->fichier) : '\0';
    if(!strcmp(mode,LECTURE) && file->tampon == -1) //Cas où on a 0xFF au début de seq
      file->tampon = fgetc(file->fichier);

  }
  return file;
}

int bstop(BFILE *fichier)
{
  if(!fichier) return 1;
  if(!strcmp(fichier->mode, ECRITURE)) // Si on est en mode écriture
  {
    if(1 + fichier->bit_index != 0)
    {
      int8_t padding = 1 + fichier->bit_index;
      fprintf(fichier->fichier, "%c%c",0xFF,padding);
    }
    fprintf(fichier->fichier, "%c", fichier->tampon);
    if(fichier->tampon == -1)
    {
      fprintf(fichier->fichier, "%c", fichier->tampon);
    }
    fprintf(fichier->fichier,"%c%c",0xFF,0x0);
  }
  free(fichier);
  return 0;
}

char bitread(BFILE *fichier)
{
  if(!fichier || strcmp(fichier->mode,LECTURE) != 0) return -1;
  static int padding = -1;
  if(fichier->bit_index < 0)
  {
    fichier->tampon = fgetc(fichier->fichier);
    if(fichier->tampon == -1)
    {
      fichier->tampon = fgetc(fichier->fichier);
      if(fichier->tampon == 0x0)
      {
        fichier->eof = 1;
        return -1;
      }
      if(fichier->tampon != -1 && fichier->tampon > 0x0 && fichier->tampon <= 0x7)
      {
        padding = fichier->tampon;
        fichier->tampon = fgetc(fichier->fichier);
      }
    }
    fichier->bit_index = 7;
  }
  if(feof(fichier->fichier) || padding == 0 || padding > fichier->bit_index)
  {
    fichier->eof = 1;
    return -1;
  }
  char current_bit = get_bit(fichier->tampon, fichier->bit_index) + '0';
  fichier->bit_index--;
  return current_bit;
}

int bitwrite(BFILE *fichier, char bit)
{
  if(!fichier || strcmp(fichier->mode,ECRITURE) != 0) return -1;
  if(fichier->bit_index < 0)
  {
    fprintf(fichier->fichier, "%c", fichier->tampon);
    if(fichier->tampon == -1)
      fprintf(fichier->fichier, "%c", fichier->tampon);
    fichier->tampon = '\0';
    fichier->bit_index = 7;
  }
  if(bit == '1')
    fichier->tampon = set_bit(fichier->tampon,fichier->bit_index);
  fichier->bit_index--;
  return 1;
}

int beof(BFILE *fichier)
{
  return fichier != NULL && fichier->eof;
}
