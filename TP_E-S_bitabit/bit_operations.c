#include "bit_operations.h"

int32_t get_bit(int32_t bits, int index)
{
  return (bits >> index) & 1;
}
int32_t set_bit(int32_t bits, int nombre)
{
  bits = bits | (1 << nombre);
  return bits;
}

int32_t clr_bit(int32_t bits, int nombre)
{
  bits = bits & ~(1 << nombre);
  return bits;
}
