#ifndef _BIT_OPERATIONS_H_
#define _BIT_OPERATIONS_H_

#include <stdint.h>

int32_t get_bit(int32_t bits, int index);

int32_t set_bit(int32_t bits, int nombre);

int32_t clr_bit(int32_t bits, int nombre);

#endif //_BIT_OPERATIONS_H_
