#!/usr/bin/env bash
clear
printf "MENU D'ACTIONS - Tests E/S bit à bit\n
\t Tests disponibles : \n
\t\t * aleat              : Sequence aleatoire\n
\t\t * seq1               : Sequence de 1\n
\t\t * seq0               : Sequence de 0\n
\t\t * seqvide            : Sequence vide\n
\t\t * term0, ..., term 7 : Terminaison de 0 à 7 bits\n
\t\t * term81             : Terminaison de huit 1\n

Entrez un nom de test (parmi les choix possibles) puis appuyez sur Entrée : "

#..........................................................................
# saisie d une touche et gestion
#..........................................................................
read answer
clear

case $answer in
  aleat)
  ./generate_sequence > sequence
  ;;
  *)
  cp sequences/$answer sequence
  ;;
esac
cat sequence
./test sequence
if diff decoded_sequence sequence > /dev/null
then
  echo Test passed, bfile seems to work for this case
else
  echo "Sequences differ, bfile does not work"
  exit 1
fi
