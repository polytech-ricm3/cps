#include <stdio.h>
#include <stdlib.h>

#include "bfile.h"


void code(char *seq)
{
    BFILE *bfichier;
    int c;
    FILE *f = fopen(seq,"r");
    FILE *output = fopen("coded_sequence","w");

    bfichier = bstart(output,"w");
    if (bfichier == NULL)
    {
        fprintf(stderr,"Erreur d'ouverture d'acces binaire en ecriture\n");
        exit(3);
    }
    c = fgetc(f);
    while (!feof(f))
    {
    if(c == '0' || c == '1')
        bitwrite(bfichier,c);
    c = fgetc(f);
    }
    bstop(bfichier);
    fclose(f);
    fclose(output);
}

void decode(char *code)
{
    BFILE *bfichier;
    char bit;
    FILE *f = fopen(code,"r");
    FILE *output = fopen("decoded_sequence","w");

    bfichier = bstart(f,"r");
    if (bfichier == NULL)
    {
        fprintf(stderr,"Erreur d'ouverture d'acces binaire en lecture\n");
        exit(3);
    }
    bit = bitread(bfichier);
    while (!beof(bfichier))
    {
        if(bit == '0' || bit == '1') fprintf(output,"%c",bit);
        bit = bitread(bfichier);
    }
    fprintf(output,"\n");
    printf("\n");
    bstop(bfichier);
    fclose(f);
    fclose(output);
}

int main(int argc, char **argv)
{
    if(argc != 2)
    {
        fprintf(stderr,"Il faut le fichier de sequence!\n");
        return 1;
    }
    code(argv[1]);
    decode("coded_sequence");
    return 0;
}
