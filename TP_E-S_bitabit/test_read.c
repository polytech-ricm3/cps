#include "bfile.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
  BFILE *bfichier;
  char bit;

  bfichier = bstart(stdin,"r");
  if (bfichier == NULL)
    {
      fprintf(stderr,"Erreur d'ouverture d'acces binaire en lecture\n");
      exit(3);
    }
  bit = bitread(bfichier);
  while (!beof(bfichier))
    {
      if(bit == '0' || bit == '1') printf("%c",bit);
      bit = bitread(bfichier);
    }
  printf("\n");
  bstop(bfichier);
  return 0;
}
