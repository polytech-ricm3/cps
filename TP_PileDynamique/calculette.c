#include "commandes_calculette.h"
#include "type_pile_erreurs.h"

void print_erreur(char *msg) { fprintf(stderr, "ERREUR : %s\n", msg); }

#define SWITCH_ERREUR(var) switch (var)
#define CASE_ERREUR(val, msg)                                                  \
    case (val):                                                                \
        print_erreur(msg);                                                     \
        break
#define DEFAULT_ERREUR(op)                                                     \
    case OK:                                                                   \
        break;                                                                 \
    default:                                                                   \
        print_erreur("Erreur non prévue pour " op);                            \
        break

int main(int argc, char **argv) {
    FILE *fichier = NULL;
    if (argc > 2) {
        printf("mauvaise utilisation : donnez un nom de fichier en argument\n");
        return 1;
    }
    if (argc == 2) {
        fichier = ouvrir_commandes(argv[1]);
    } else {
        fichier = stdin;
    }
    PileEntiers *p; //= creer_pile();//= malloc(sizeof(PileEntiers));
    //creer_pile(p);

    commande command;

    int value1, value2;
    erreur_pile err;

    while (!fin_commandes(fichier)) {
        command = commande_suivante(fichier);
        print_commandes(command);
        switch (command.cmd) {
        case VIDER_PILE:
            err = vider(&p);
            SWITCH_ERREUR(err) {
                CASE_ERREUR(POINTEUR_NUL, "vider une pile sur pointeur nul");
                DEFAULT_ERREUR("vider une pile");
            }
            break;
        case DEPILER_SOMMET:
            err = depiler(&p);
            SWITCH_ERREUR(err) {
                CASE_ERREUR(POINTEUR_NUL, "dépiler sur pointeur nul");
                CASE_ERREUR(PILE_VIDE, "dépiler sur pile vide");
                DEFAULT_ERREUR("dépiler");
            }
            break;
        case EMPILER_VALEUR:
            err = empiler(&p, command.arg);
            SWITCH_ERREUR(err) {
                CASE_ERREUR(POINTEUR_NUL, "empiler sur pointeur nul");
                CASE_ERREUR(PILE_PLEINE, "empiler sur pile pleine");
                DEFAULT_ERREUR("empiler");
            }
            break;

#define IF_PILE_VIDE_ERREUR_ELSE(val, op, msg)                                 \
    if (err != OK) {                                                           \
        SWITCH_ERREUR(val) {                                                   \
            CASE_ERREUR(PILE_VIDE, op " : " msg);                              \
            DEFAULT_ERREUR(op);                                                \
        }                                                                      \
    } else

#define IF_GET_2_OPERANDES(v1, v2, op)                                         \
    int _if_get_2_operandes = 0;                                               \
    err = sommet(p, &(v2));                                                    \
    IF_PILE_VIDE_ERREUR_ELSE(err, op,                                          \
                             "deuxième opérande manquant (pile vide)") {       \
        err = depiler(&p);                                                     \
        IF_PILE_VIDE_ERREUR_ELSE(                                              \
            err, op, "depiler sur pile vide (deuxième opérande)") {            \
            err = sommet(p, &(v1));                                            \
            IF_PILE_VIDE_ERREUR_ELSE(                                          \
                err, op, "premier opérande manquant (pile vide)") {            \
                err = depiler(&p);                                             \
                IF_PILE_VIDE_ERREUR_ELSE(                                      \
                    err, op, "depiler sur pile vide (premier opérande)") {     \
                    _if_get_2_operandes = 1;                                   \
                }                                                              \
            }                                                                  \
        }                                                                      \
    }                                                                          \
    if (_if_get_2_operandes)

#define GESTION_RESULTAT(val, op)                                              \
    err = empiler(&p, (val));                                                  \
    SWITCH_ERREUR(err) {                                                       \
        CASE_ERREUR(POINTEUR_NUL, op " : empiler sur pointeur nul");           \
        CASE_ERREUR(PILE_PLEINE, op " : empiler sur pile pleine");             \
        DEFAULT_ERREUR(op " : empiler");                                       \
    }
        case EFFECTUER_ADDITION: {
            IF_GET_2_OPERANDES(value1, value2, "addition") {
                GESTION_RESULTAT(value1 + value2, "addition");
            }
        } break;
        case EFFECTUER_SOUSTRACTION: {
            IF_GET_2_OPERANDES(value1, value2, "soustraction") {
                GESTION_RESULTAT(value1 - value2, "soustraction");
            }
        } break;
        case EFFECTUER_MULTIPLICATION: {
            IF_GET_2_OPERANDES(value1, value2, "multiplication") {
                GESTION_RESULTAT(value1 * value2, "multiplication");
            }
        } break;
        case EFFECTUER_DIVISION: {
            IF_GET_2_OPERANDES(value1, value2, "division") {
                if (value2 == 0) {
                    print_erreur("division : division par 0");
                } else {
                    GESTION_RESULTAT(value1 / value2, "division");
                }
            }
        } break;
        case COMMANDE_INCORRECTE:
            break;
        case QUITTER:
	    vider(&p);
            break;
        }
        print(p);
        printf("\n");
    }
    fermer_commandes(fichier);
    return 0;
}
