#include "type_pile_erreurs.h"
#include <stdio.h>
#include <stdlib.h>

/* Constructeurs */

/* Créer une pile vide */
PileEntiers *creer_pile() {
    return malloc(sizeof(PileEntiers));
}

/* Opérations d'accès */

/* Retourne vrai ssi p est vide */
int est_vide(PileEntiers *p) { return (p == NULL); }

/* Renvoie l'entier en haut de la pile */
/* p doit être non vide */
erreur_pile sommet(PileEntiers *p, int *res) {
    if(p == NULL)
        return POINTEUR_NUL;
    if (!est_vide(p)) {
        *res = p->val;
        return OK;
    } else {
        return PILE_VIDE;
    }
}

/* Afficher les éléments de la pile */
void print(PileEntiers *p) {
    printf("[ ");
    while(p != NULL)
    {
        printf("%i ",p->val);
        p = p->prec;
    }
    printf("]\n");
}

/* Opérations de modification */

/* Vider la pile p */
erreur_pile vider(PileEntiers **p) {
    if(p == NULL)
        return POINTEUR_NUL;
    PileEntiers *courrant = (*p);
    PileEntiers *suiv;
    while(courrant != NULL)
    {
        suiv = courrant->prec;
        free(courrant);
        courrant = suiv;
    }
    free(p);
   // p = NULL;
    return OK;
}

/* Empiler un entier x */
/* Précondition : taille(p) < TAILLE_MAX */
erreur_pile empiler(PileEntiers **p, int x) {
    if((*p) == NULL)
    {
        (*p) = malloc(sizeof(PileEntiers));
        (*p)->val = x;
        return OK;
    }
    PileEntiers *new = malloc(sizeof(PileEntiers));
    new->val = x;
    new->prec = (*p);
    (*p) = new;
   // print(p);
    return OK;
}

/* Supprimer l'entier en haut de la pile */
/* Précondition : p non vide */
erreur_pile depiler(PileEntiers **p) {
    if (!est_vide((*p))) {
        (*p) = (*p)->prec;
        return OK;
    } else {
        return PILE_VIDE;
    }
}
