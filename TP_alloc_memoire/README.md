# TP Allocateur mémoire

## Contenu du package :
- Un petit programme de test interactif de l'allocateur qui devra être implémenté dans mem.c : memshell
- Un petit programme contenant un test simple de l'initialisation de l'allocateur qui devra être implémenté dans mem.c : test_init
- Un Makefile vous permettant de compiler tout ces petits programmes et de tester votre allocateur avec une appli réelle (`make test_ls` ou `make test`)

> **ATTENTION:** sans implémentation correcte du début de l'allocateur, test_init boucle indéfiniment.
