#include "mem.h"
#include "common.h"

#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

// constante définie dans gcc seulement
#ifdef __BIGGEST_ALIGNMENT__
#define ALIGNMENT __BIGGEST_ALIGNMENT__
#else
#define ALIGNMENT 16
#endif

#define ALIGN(val,align) \
((((__intptr_t)(val) + ((align) - 1)) & ~ ((align)-1)))

#define max(a,b) ((a)>(b)?(1):(0))
#define min(a,b) ((a)<(b)?(1):(0))

//Pointeur vers la première zone libre de notre mémoire
static struct fb *zl;

/*
* Fonction initialisant la mémoire en début d'exécution
* void* mem : Adresse de la zone mémoire à initialiser
* size_t taille : Taille de la mémoire souhaitée
*/
void
mem_init (void *mem, size_t taille)
{
	assert (mem == get_memory_adr ());
	assert (taille == get_memory_size ());
	zl = (struct fb *) mem;	//On itialise notre pointeur sur la première zone libre
	// Qui correspond au début de la mémoire lorsqu'il n'y a pas encore eu d'allocation
	zl->size = taille;
	zl->next = NULL;
	mem_fit (&mem_fit_first);
}

/*
* Fonction permettant de parcourir et afficher l'etat de toutes les zones en mémoire
* void (*print)(void *, size_t, int) : Pointeur sur la fonction d'affichage
*/
void
mem_show (void (*print) (void *, size_t, int))
{
	void *current_block = get_memory_adr ();	//Première zone de la mémoire
	void *next_free_block = zl;	// Première zone_libre
	void *end_mem = current_block + get_memory_size ();	//Fin de la mémoire

	while (current_block < end_mem)	//On parcourt la zone mémoire
	{
		int is_free_block = (next_free_block != NULL && current_block == next_free_block);	//On regarde si la zone actuelle est une zone libre
		size_t size = mem_get_size (current_block);

		print (current_block, size, is_free_block);
		if (is_free_block)
		{
			next_free_block = ((struct fb *) current_block)->next;	//On met à jour le pointeur sur la zone libre suivante
		}
		current_block = (void *) current_block + size;	//On passe à la zone suivante
	}
}

/*
* Fonction permettant de choisir la fonction de recherche
* mem_fit_function_t *f : Pointeur vers la fonction de recherche
*/
static mem_fit_function_t *mem_fit_fn;
void
mem_fit (mem_fit_function_t * f)
{
	mem_fit_fn = f;
}

/*
* Fonction permettant d'allouer une zone en mémoire
* size_t taille : Taille de la zone à allouer
*/
void *
mem_alloc (size_t taille)
{
	if (taille <= 0 || taille > get_memory_size () - sizeof (struct fb))
	return NULL;
	__attribute__ ((unused))	/* juste pour que gcc compile ce squelette avec -Werror */
	size_t needed_size = ALIGN (taille + sizeof (struct fb), ALIGNMENT);	// On s'assure que la taille minimum respecte bien l'alignement nécessaire
	struct fb *fb = mem_fit_fn (zl, needed_size);	//On cherche la première zone libre respectant la politique choisie

	if (!fb)
	return NULL;		//Si jamais il n'y a pas de zone correspondant aux critères on retourne NULL

	struct fb *zo = NULL;		//Sinon on initialise ce qui deviendra la zone occupée

	if (fb == zl)			//Cas de la première allocation
	{
		if (zl->size > needed_size)	// Si jamais il y a encore de la place après l'allocation
		{
			zo = ((struct fb *) (((void *) fb) + fb->size - needed_size));
			zo->size = needed_size;
			fb->size = fb->size - needed_size;
		}
		else if ((zl->size - needed_size) == 0)	// Cas de il n'y a plus de zone libre après l'allocation
		{
			zo = ((struct fb *) (((void *) fb) + fb->size - needed_size));
			zo->size = needed_size;
			zl = NULL;
		}
	}
	else				// Autres cas
	{
		struct fb *current = zl;
		while (current->next != fb)	//On avance jusqu'à arriver à la bonne zone libre
		{
			current = current->next;
		}
		zo = (struct fb *) ((void *) fb + fb->size - needed_size);
		zo->size = needed_size;
		fb->size = fb->size - needed_size;
		if (fb->size == 0)	//Si jamais la zone était pile à la bonne taille
		{
			current->next = fb->next;
			fb->next = NULL;
		}
	}
	return zo;
}


void
mem_free (void *mem)
{

	void *end_mem = get_memory_adr () + get_memory_size ();	// Récupération fin de memeoire

	if (mem >= end_mem)
	return;			// L'adresse passée est hors mémoire

	size_t area_size = mem_get_size (mem);
	struct fb *current = zl;
	if (zl == NULL)		// Toute la mémoire est occuppée
	{
		zl = ((struct fb *) mem);
		zl->size = area_size;
	}
	else
	{
		if (zl->size == get_memory_size ())
		return;			// Toute la mémoire est libre

		struct fb *nxt_free_area = zl;
		struct fb *prv_area_mem;
		current = get_memory_adr ();
		while (current != NULL && current < ((struct fb *) end_mem)
		&& current != ((struct fb *) mem))
		{
			/* Boucle de recherche de la zone à libérer ainsi que les zones libres à proximité (avant/après la zone libérer) */
			if (current == nxt_free_area && nxt_free_area->next != NULL)
			{
				if (((void *) current) + mem_get_size ((void *) current) == mem)
				prv_area_mem = current;
				nxt_free_area = nxt_free_area->next;
			}
			current = ((struct fb *) (((void *) current) + current->size));
		}

		if (current == ((struct fb *) mem) && current == nxt_free_area)
		return;			// Zone déjà libre
		current = ((struct fb *) mem);

		if (((void *) current) + mem_get_size ((void *) current) ==
		((void *) nxt_free_area))
		{
			current->size += nxt_free_area->size;
			current->next = nxt_free_area->next;
			if (((void *) zl) > ((void *) current) && zl == nxt_free_area)
			zl = current;
			nxt_free_area->next = NULL;
		}
		else
		if ((((void *) nxt_free_area) +
		mem_get_size ((void *) nxt_free_area)) == current)
		{
			nxt_free_area->size += current->size;
		}
		else
		{
			nxt_free_area->next = current;
		}
		if (prv_area_mem != NULL)	// Cas où la zone juste avant est libre --> fusion
		{
			prv_area_mem->size += current->size;
			prv_area_mem->next = current->next;
		}
	}
}


struct fb *
mem_fit_first (struct fb *list, size_t size)
{
	struct fb *current_block = list;	//On parcourt uniquement des zones libres

	while (current_block != NULL && current_block->size < size)	//On s'arrête dès qu'une zone est capable d'accueillir la zone
	{
		current_block = current_block->next;
	}
	return current_block;
}

/* Fonction à faire dans un second temps
* - utilisée par realloc() dans malloc_stub.c
* - nécessaire pour remplacer l'allocateur de la libc
* - donc nécessaire pour 'make test_ls'
* Lire malloc_stub.c pour comprendre son utilisation
* (ou en discuter avec l'enseignant)
*/
size_t
mem_get_size (void *zone)
{
	/* zone est une adresse qui a été retournée par mem_alloc() */

	/* la valeur retournée doit être la taille maximale que
	* l'utilisateur peut utiliser dans cette zone */
	return ((struct fb *) zone)->size;
}

/* Fonctions facultatives
* autres stratégies d'allocation
*/
struct fb *
mem_fit_best (struct fb *list, size_t size)
{
	struct fb *current_block = list;
	struct fb *best_fit = current_block;

	while (current_block != NULL)
	{
		if (current_block->size >= size && min (current_block->size, best_fit->size))	//On veut la plus petite zone capable d'accueillir size
		{
			best_fit = current_block;
		}
		current_block = current_block->next;
	}
	return best_fit;
}

struct fb *
mem_fit_worst (struct fb *list, size_t size)
{
	struct fb *current_block = list;
	struct fb *worst_fit = current_block;

	while (current_block != NULL)
	{
		if (current_block->size >= size && max (current_block->size, worst_fit->size))	//On veut la plus grande zone capable d'accueillir size
		{
			worst_fit = current_block;
		}
		current_block = current_block->next;
	}
	return worst_fit;
}

struct fb* get_zl() {
	return zl;
}
