#include "mem.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
  adresse - get_memory_adr(), (unsigned long) taille);
}

int main(int argc, char **argv)
{
  printf("\n\n-----------------------TEST Allocation de toute la mémoire en 1 unique bloc-------------------------\n\n");
  mem_init(get_memory_adr(), get_memory_size());
  //Récupération de la taille maximale allouable
  printf("Allocation de toute la mémoire : \n\n");
  void *addr = alloc_max(get_memory_size());

  mem_show(afficher_zone);
  printf("\n");
  assert(addr == get_memory_adr() && get_zl() == NULL);
  printf("Libération de toute la mémoire : \n\n");
  mem_free(addr);
  mem_show(afficher_zone);
  assert(get_zl() == get_memory_adr() && get_zl()->size == get_memory_size());



  printf("\n\n----------------------TEST Allocation de toute la mémoire en plusieurs blocs------------------------\n\n");
  mem_init(get_memory_adr(), get_memory_size());
  //MEM complete à plusieurs zones
  size_t sizes[] = {10,20,20};
  int nb_alloc = sizeof(sizes)/sizeof(size_t);
  int addrs[nb_alloc];

  printf(" - Allocation de la mémoire en bloc de 10, 20 et 20: \n\n");
  for(int i = 0; i < nb_alloc; i++)
  {
    void *ptr = mem_alloc(sizes[i]);
    addrs[i] = (int) (ptr - get_memory_adr());
    if(ptr == NULL)
      printf("\t- Allocation impossible\n");
  }
  mem_show(afficher_zone);
  printf("\n");

  printf(" - Libération de la mémoire\n\n");

  printf(" - Libération en milieu de mémoire\n\n");
  mem_free(get_memory_adr() + addrs[1]);
  mem_show(afficher_zone);
  printf("\n");

  printf(" - Libération du voisin droit\n\n");
  fflush(stdout);
  mem_free(get_memory_adr() + addrs[0]);
  mem_show(afficher_zone);
  printf("\n");

  printf(" - Libération du voisin gauche\n\n");
  fflush(stdout);
  mem_free(get_memory_adr() + addrs[2]);
  mem_show(afficher_zone);
  printf("\n");


  printf("\n\n --------------- TEST Liberation dans le meme ordre d'allocation ----------------------\n\n");
  for(int i = 0; i < nb_alloc; i++)
  {
    void *ptr = mem_alloc(sizes[i]);
    addrs[i] = (int) (ptr - get_memory_adr());
    if(ptr == NULL)
      printf("\t- Allocation impossible\n");
  }

  for(int i = 0; i < nb_alloc; i++)
  {
    fflush(stdout);
    mem_free(get_memory_adr() + addrs[i]);
    mem_show(afficher_zone);
    printf("\n");
  }

  printf("\n\n --------------- TEST Liberation dans l'ordre inverse d'allocation ----------------------\n\n");
  for(int i = 0; i < nb_alloc; i++)
  {
    void *ptr = mem_alloc(sizes[i]);
    addrs[i] = (int) (ptr - get_memory_adr());
    if(ptr == NULL)
      printf("\t- Allocation impossible\n");
  }

  for(int i = nb_alloc - 1; i >= 0; i--)
  {
    fflush(stdout);
    mem_free(get_memory_adr() + addrs[i]);
    mem_show(afficher_zone);
    printf("\n");
  }

  assert(get_zl() == get_memory_adr() && get_zl()->size == get_memory_size());

  printf("\n\n----------------------TEST Allocation de valeurs nulles ou négatives ------------------------\n\n");
  mem_init(get_memory_adr(), get_memory_size());
  void *ptr = mem_alloc(0);
  printf("Test d'allocation avec une taille à 0 : ");
  assert(ptr == NULL);
  printf("OK\n");
  mem_show(afficher_zone);
  printf("\n");

  ptr = mem_alloc(-5);
  printf("Test d'allocation avec une taille à -5 :");
  assert(ptr == NULL);
  printf(" OK\n");
  mem_show(afficher_zone);
  printf("\n");

  printf("Test de libération d'une zone déjà libre :");
  fflush(stdout);
  mem_free(get_memory_adr());
  assert(get_zl() == get_memory_adr() && get_zl()->size == get_memory_size());
  printf(" OK\n");
  mem_show(afficher_zone);
  printf("\n");
  printf("\n\n----------------------FIN------------------------\n\n");
}
